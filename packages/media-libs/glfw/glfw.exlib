# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='glfw' ] cmake

export_exlib_phases src_install

SUMMARY="A multi-platform library for OpenGL, OpenGL ES, Vulkan, window and input"
DESCRIPTION=""
HOMEPAGE="https://glfw.org/"

LICENCES="ZLIB"
SLOT="0"
MYOPTIONS="
    doc
    ( X wayland ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        kde-frameworks/extra-cmake-modules
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        sys-libs/vulkan-headers
        sys-libs/vulkan-loader
        x11-dri/mesa[wayland?][X?]
        x11-libs/libxkbcommon[X?]
        wayland? (
            sys-libs/wayland
            sys-libs/wayland-protocols[>=1.15]
        )
        X? (
            x11-libs/libX11
            x11-libs/libXcursor
            x11-libs/libXinerama
            x11-libs/libXrandr
        )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS=ON
    -DGLFW_BUILD_EXAMPLES=OFF
    -DGLFW_BUILD_TESTS=OFF
    -DGLFW_VULKAN_STATIC=OFF
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'doc GLFW_BUILD_DOCS'
    'wayland GLFW_USE_WAYLAND'
)

glfw_src_install() {
    cmake_src_install

    if option doc ; then
        edo pushd docs
        # Install Doxygen generated documentation
        dodoc -r html
        edo popd
    fi
}

