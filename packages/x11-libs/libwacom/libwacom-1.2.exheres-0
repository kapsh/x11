# Copyright 2011 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=linuxwacom release=${PNV} suffix=tar.bz2 ] \
    meson \
    udev-rules

SUMMARY="Library to identify wacom tabs and their model-specific features"
DESCRIPTION="
libwacom is a library to identify wacom tablets and their model-specific
features. It provides easy access to information such as 'is this a built-in
on-screen tablet', 'what is the size of this model', etc.
"
HOMEPAGE+=" https://linuxwacom.github.io"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/glib:2[>=2.36]
        gnome-desktop/libgudev
    test:
        dev-libs/libxml2:2.0
        gnome-desktop/librsvg:2
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddocumentation=disabled
    -Dudev-dir=${UDEVDIR}
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_install() {
    meson_src_install

    if option doc ; then
        dodoc -r doc/html
    fi
}

